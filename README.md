# WLM map

# Installation

- `git clone` this repository somewhere, then `cd` into it
- `poetry shell` will install create the virtualenv, install the
dependencies and activate the virtualenv shell

# Run
- `python generate_map.py wlm` or `wle`  will generate the maps that will appear
in the outputs folder
