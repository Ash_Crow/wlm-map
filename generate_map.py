import argparse
from bs4 import BeautifulSoup
from contextlib import closing
import csv
import io
import requests

from utils import get_color, get_country_code


class MapGenerator:
    first_year = 0
    latest_year = 0
    max_participations = 0
    participations_by_country = {}
    participations_by_count = {}

    data_url = "https://public-paws.wmcloud.org/User:Effeietsanders/output/basic_stats/all_uploads_year_and_country.csv"  # noqa

    first_year = 0

    user_agent = "WLM Map/0.0 (https://framagit.org/Ash_Crow/wlm-map; sylvain@boissel.dev)"  # noqa

    def __init__(self, project):
        self.project = project

        # Color codes obtained with https://convertingcolors.com/
        if project == "wlm":
            self.color_code_percent = (0.6, 0.08, 0.11)
            self.color_code_hex = "#99141b"
        elif project == "wle":
            self.color_code_percent = (0.2, 0.6, 0.4)
            self.color_code_hex = "#339966"
        else:
            raise ValueError("Please choose a valid project")

    def get_data_from_url(self) -> None:
        with closing(
            requests.get(
                self.data_url, headers={"User-Agent": self.user_agent}, stream=True
            )
        ) as response:
            response.encoding = response.apparent_encoding
            file_data = io.StringIO(response.text)
            self.reader = csv.DictReader(file_data)

    def get_participations_by_country(self) -> None:
        for row in self.reader:
            if row["activity"] == project:
                country = row["subcat_country"].replace("_", " ")
                year = int(row["year"])

                if self.first_year == 0 or year < self.first_year:
                    self.first_year = year

                if year > self.latest_year:
                    self.latest_year = year

                country_code = get_country_code(country)
                if country_code not in self.participations_by_country:
                    self.participations_by_country[country_code] = 1
                else:
                    self.participations_by_country[country_code] += 1

    def get_participations_by_count(self) -> None:
        participations_by_count = {}
        for country_code, count in self.participations_by_country.items():
            if count not in participations_by_count:
                participations_by_count[count] = []

            participations_by_count[count].append(country_code)

            if count > self.max_participations:
                self.max_participations = count

        self.participations_by_count = dict(sorted(participations_by_count.items()))

    def format_gradient(self) -> str:
        outputs = []
        for count, country_codes in self.participations_by_count.items():
            if count == 1:
                outputs.append(f"/* {count} participation */")
            else:
                outputs.append(f"/* {count} participations */")

            color = get_color(
                self.color_code_percent, self.max_participations + 1, count
            )
            outputs.append(
                f".{', .'.join(country_codes)} {{ opacity:1; fill:{color}; }}"
            )

        return "\n".join(outputs)

    def format_monochrome(self) -> str:
        country_codes = self.participations_by_country.keys()
        return f".{', .'.join(country_codes)} {{ opacity:1; fill:{self.color_code_hex}; }}"  # noqa

    def update_map(self, mode: str) -> None:
        if mode == "monochrome":
            file_name = f"{self.project}_world_map_monochrome.svg"
        else:
            file_name = f"{self.project}_world_map_by_number_of_participations.svg"

        with open(f"source_maps/{file_name}") as source_file:
            svg = source_file.read()

        soup = BeautifulSoup(svg, "xml")
        svg_tag = soup.find("svg")
        label = svg_tag.find("text", {"id": "label"}).find("tspan")

        updated_label = soup.new_tag("tspan")
        updated_label.string = f"{self.first_year} — {self.latest_year}"
        label.replace_with(updated_label)

        style_sheet = soup.find("style", {"id": "style_css_sheet"})
        style = style_sheet.text

        contest_data_start = "/* Enter contest data here */"
        updated_style = soup.new_tag("style")

        if mode == "monochrome":
            contest_data = self.format_monochrome()
        else:
            contest_data = self.format_gradient()

        updated_style.string = style.replace(
            contest_data_start, contest_data_start + "\n" + contest_data
        )
        style_sheet.replace_with(updated_style)

        with open(f"outputs/{file_name}", "w") as result_file:
            result_file.write(soup.prettify())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate a list of country code for the map."
    )
    parser.add_argument("project", type=str, nargs="+", help="The project (wlm, wle)")

    args = parser.parse_args()

    project = args.project[0]

    print(f"Processing data for project {project}")

    map_generator = MapGenerator(project)

    map_generator.get_data_from_url()
    map_generator.get_participations_by_country()

    map_generator.get_participations_by_count()

    map_generator.update_map("monochrome")
    map_generator.update_map("gradient")
