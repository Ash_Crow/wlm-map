import pycountry
from matplotlib.colors import LinearSegmentedColormap, rgb2hex


def get_country_code(country_name: str) -> str:
    """
    Takes the name of the country and returns the 2-char version
    of the ISO-3166-1 country code
    """
    # Managing countries that do not match in pycountry even with fuzzy search
    if country_name == "Bosnia-Herzegovina":
        country_code = "ba"
    elif country_name == "Dutch Caribbean":
        country_code = "bq"
    elif country_name == "Democratic Republic of the Congo":
        country_code = "cd"
    else:
        country_entry = pycountry.countries.search_fuzzy(country_name)[0]
        country_code = country_entry.alpha_2.lower()

    return country_code


def get_color(end_color: tuple, steps: int, index: int) -> str:
    """
    Returns the corresponding color
    """
    colors = [(1, 1, 1), end_color]
    cm = LinearSegmentedColormap.from_list("Custom", colors, N=steps)
    color_list = [rgb2hex(cm(i)) for i in range(cm.N)]

    return color_list[index]
